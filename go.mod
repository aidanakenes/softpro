module aitu.kz/softpro

go 1.15

require (
	github.com/bmizerany/pat v0.0.0-20210406213842-e4b6760bdd6f
	github.com/golangcollege/sessions v1.2.0
	github.com/jackc/pgx v3.6.2+incompatible // indirect
	github.com/jackc/pgx/v4 v4.13.0
	github.com/justinas/alice v1.2.0
	github.com/pkg/errors v0.9.1 // indirect
)
