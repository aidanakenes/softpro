package postgres

import (
	"aitu.kz/softpro/pkg/models"
	"context"
	"database/sql"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"strconv"
	"strings"
	"time"
)

const (
	insertSql                 = "INSERT INTO snippets (title,content,created,expires,user_id) VALUES ($1,$2,$3,$4,$5) RETURNING id"
	getSnippetById            = "SELECT id, title, content, created, expires FROM snippets where id=$1"
	getLastTenCreatedSnippets = "SELECT id, title, content, created, expires FROM snippets WHERE expires > now() and user_id=$1 ORDER BY created DESC LIMIT 10"
	auth                      = "SELECT id, hashed_password FROM users WHERE email = $1"
	createUser                = "INSERT INTO users(name,email,hashed_password, created,active) VALUES ($1,$2,$3,$4,$5)"
	deleteSnippetById         = "DELETE FROM snippets WHERE id =$1"
	updateSnippet             = "UPDATE snippets SET title = $1, content= $2 WHERE id = $3"
)

type SnippetModel struct {
	Pool *pgxpool.Pool
}

var userId int

func (m *SnippetModel) Insert(title, content, expires string) (int, error) {
	now := time.Now()
	var id uint64

	exp, _ := strconv.Atoi(expires)
	after := now.AddDate(exp, 0, 0)
	row := m.Pool.QueryRow(context.Background(), insertSql, title, content, time.Now(), after, userId)
	err := row.Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}
func (m *SnippetModel) CreateUser(name, email, password string) error {
	now := time.Now()

	m.Pool.QueryRow(context.Background(), createUser, name, email, password, now, true)

	return nil
}

func (m *SnippetModel) Get(id int) (*models.Snippet, error) {
	s := &models.Snippet{}
	err := m.Pool.QueryRow(context.Background(), getSnippetById, id).
		Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {
		if err.Error() == "no rows in result set" {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return s, nil
}

func (m *SnippetModel) Latest() ([]*models.Snippet, error) {
	snippets := []*models.Snippet{}
	rows, err := m.Pool.Query(context.Background(), getLastTenCreatedSnippets, userId)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		s := &models.Snippet{}
		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err
		}
		snippets = append(snippets, s)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return snippets, nil
}

func (m *SnippetModel) Authenticate(email, password string) (int, error) {
	var id int
	var pass string
	row := m.Pool.QueryRow(context.Background(), auth, email)
	err := row.Scan(&id, &pass)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, err
		}
	}

	if strings.Trim(pass, " ") != password {
		return 0, models.ErrInvalidCredentials
	}
	userId = id
	return id, nil
}

func (m *SnippetModel) DeleteStudentById(id int) error {
	_, err := m.Pool.Exec(context.Background(), deleteSnippetById, id)
	if err != nil {
		return err
	}

	return nil
}

func (m *SnippetModel) UpdateSnippet(title, content string, id int) error {
	_, err := m.Pool.Exec(context.Background(), updateSnippet, title, content, id)

	if err != nil {
		return err
	}
	return nil

}

